/*
 * Public API Surface of entry point kypo-training-agenda/adaptive-definition-summary
 */

export * from './components/adaptive-definition-summary-components.module';
export * from './components/adaptive-definition-summary.component';
