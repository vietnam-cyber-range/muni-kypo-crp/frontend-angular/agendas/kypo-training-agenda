/*
 * Public API Surface of entry point kypo-adaptive-agenda/run-results
 */
export * from './components/adaptive-run-results-components.module';
export * from './components/adaptive-run-results.component';
