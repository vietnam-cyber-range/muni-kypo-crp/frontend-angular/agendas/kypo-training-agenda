import { TrainingDefinition } from '@muni-kypo-crp/training-model';

export class TrainingDefinitionRowAdapter extends TrainingDefinition {
  createdAtFormatted: string;
}
