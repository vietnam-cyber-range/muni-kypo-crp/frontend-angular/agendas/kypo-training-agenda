/*
 * Public API Surface of entry point kypo-training-agenda/definition-summary
 */

export * from './components/training-definition-summary-components.module';
export * from './components/training-definition-summary.component';
