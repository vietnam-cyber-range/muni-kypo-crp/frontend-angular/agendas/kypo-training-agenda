/*
 * Public API Surface of entry point kypo-training-agenda/instance-access-token
 */

export * from './components/access-token-detail-components.module';
export * from './components/access-token-detail.component';
