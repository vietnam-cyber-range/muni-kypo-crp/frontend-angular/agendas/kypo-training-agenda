/*
 * Public API Surface of entry point kypo-training-agenda/instance-cheating-detection
 */
export * from './components/cheating-detection-overview-components.module';
export * from './components/cheating-detection-overview.component';
export * from './services/cheating-detection.service';
