/*
 * Public API Surface of entry point kypo-training-agenda/instance-detail
 */
export * from './components/training-instance-detail-components.module';
export * from './components/adaptive-instance-detail-components.module';
