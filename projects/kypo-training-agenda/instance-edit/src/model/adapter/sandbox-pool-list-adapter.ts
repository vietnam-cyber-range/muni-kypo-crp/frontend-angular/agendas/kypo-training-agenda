import { Pool } from '@muni-kypo-crp/sandbox-model';

export class SandboxPoolListAdapter extends Pool {
  title: string;
}
