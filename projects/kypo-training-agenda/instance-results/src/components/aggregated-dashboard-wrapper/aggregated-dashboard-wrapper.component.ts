import { Component, OnInit } from '@angular/core';
import { SentinelBaseDirective } from '@sentinel/common';
import { TrainingInstance } from '@muni-kypo-crp/training-model';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, takeWhile } from 'rxjs';
import { TrainingNavigator } from '@muni-kypo-crp/training-agenda';
import { map } from 'rxjs/operators';

@Component({
  selector: 'kypo-aggregated-dashboard-wrapper',
  templateUrl: './aggregated-dashboard-wrapper.component.html',
  styleUrls: ['./aggregated-dashboard-wrapper.component.css'],
})
export class AggregatedDashboardWrapperComponent extends SentinelBaseDirective implements OnInit {
  trainingInstance$: Observable<TrainingInstance>;

  constructor(private activeRoute: ActivatedRoute, private router: Router, private navigator: TrainingNavigator) {
    super();
  }

  ngOnInit(): void {
    this.trainingInstance$ = this.activeRoute.parent.data.pipe(
      takeWhile(() => this.isAlive),
      map((data) => data.trainingInstance as TrainingInstance)
    );
  }

  redirectToDetailView(instanceId: number): void {
    this.router.navigate([this.navigator.toTrainingInstanceResults(instanceId)]);
  }
}
