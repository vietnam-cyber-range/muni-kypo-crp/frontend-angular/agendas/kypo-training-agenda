/*
 * Public API Surface of entry point kypo-training-agenda/run-overview
 */

export * from './components/training-run-overview-components.module';
export * from './components/training-run-overview.component';
export * from './services/state/training/accessed-training-run.service';
