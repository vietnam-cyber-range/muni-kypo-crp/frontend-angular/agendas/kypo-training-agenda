/*
 * Public API Surface of entry point kypo-training-agenda/run-results
 */
export * from './components/training-run-results-components.module';
export * from './components/training-run-results.component';
